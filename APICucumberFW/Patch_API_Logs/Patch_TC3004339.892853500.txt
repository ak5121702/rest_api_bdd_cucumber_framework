Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Akki",
    "job": "SrQA"
}

Response header date is : 
Fri, 15 Mar 2024 19:13:40 GMT

Response body is : 
{"name":"Akki","job":"SrQA","updatedAt":"2024-03-15T19:13:40.274Z"}
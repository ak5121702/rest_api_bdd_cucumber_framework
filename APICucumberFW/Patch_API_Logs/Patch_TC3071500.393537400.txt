Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Akki",
    "job": "SrQA"
}

Response header date is : 
Sat, 16 Mar 2024 01:45:00 GMT

Response body is : 
{"name":"Akki","job":"SrQA","updatedAt":"2024-03-16T01:45:00.770Z"}
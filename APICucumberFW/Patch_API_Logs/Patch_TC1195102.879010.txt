Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Vinayak",
    "job": "Lead"
}

Response header date is : 
Sat, 16 Mar 2024 14:21:02 GMT

Response body is : 
{"name":"Vinayak","job":"Lead","updatedAt":"2024-03-16T14:21:02.262Z"}
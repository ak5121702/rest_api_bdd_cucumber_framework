Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Akki",
    "job": "SrQA"
}

Response header date is : 
Sat, 16 Mar 2024 15:10:58 GMT

Response body is : 
{"name":"Akki","job":"SrQA","updatedAt":"2024-03-16T15:10:58.256Z"}
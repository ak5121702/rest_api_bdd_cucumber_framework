package Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Methods.Utility;
public class RequestBody extends Environment {

	public static String req_testcase1(String TestCaseName) throws IOException {
		ArrayList<String> Data=Utility.readExcelData("POST_API", TestCaseName);
		String key_name = Data.get(1);
		String value_name= Data.get(2);
		String key_job= Data.get(3);
		String value_job = Data.get(4);
		String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
		return req_body;
	}
	public static String req_testcase2(String TestCaseName)throws IOException {
			ArrayList<String> Data=Utility.readExcelData("PUT_API", TestCaseName);
			String key_name = Data.get(1);
			String value_name= Data.get(2);
			String key_job= Data.get(3);
			String value_job = Data.get(4);
			String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
			return req_body;
		}
	public static String req_testcase3(String TestCaseName)throws IOException {
		ArrayList<String> Data=Utility.readExcelData("PATCH_API", TestCaseName);
		String key_name = Data.get(1);
		String value_name= Data.get(2);
		String key_job= Data.get(3);
		String value_job = Data.get(4);
		String req_body = "{\r\n" + "    \""+key_name+"\": \""+value_name+"\",\r\n" + "    \""+key_job+"\": \""+value_job+"\"\r\n" + "}";
		return req_body;
	}
	public static String req_testcase4(String TestCaseName)throws IOException {
		ArrayList<String> Data=Utility.readExcelData("GET_API", TestCaseName);
		
		return TestCaseName;
		}
	
		public static String req_testcase5(String TestCaseName)throws IOException {
			ArrayList<String> Data=Utility.readExcelData("DELETE_API", TestCaseName);
			
			return TestCaseName;
	}

	


}
	





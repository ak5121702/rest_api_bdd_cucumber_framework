Feature: Trigger the Get API and validate the response body and response parameters
@Get_API_Testcases
Scenario: Trigger the Get API request with valid string request body parameters
         Given configure endpoint for Get API
         When send the request to the endpoint of Get API
         Then validate status code for Get API
         And validate response body parameters for Get API
         
         
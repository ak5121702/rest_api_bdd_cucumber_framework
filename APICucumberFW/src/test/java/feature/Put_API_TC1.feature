Feature: Trigger the Put_TC_1 and validate the response body and response parameters
@Put_API_Testcases
Scenario: Trigger the Put_TC_1 API request with valid string request body parameters
         Given Name and Job in request body for Put_TC_1
         When send the request with payload to the endpoint of Put_TC_1
         Then validate status code for Put_TC_1
         And validate response body parameters for Put_TC_1
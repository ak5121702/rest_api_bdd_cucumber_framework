Feature: Trigger the API and validate the response body and response parameters
@Post_API_Testcases
Scenario: Trigger the API request with valid string request body parameters
         Given Name and Job in request body
         When send the request with payload to the endpoint
         Then validate status code
         And validate response body parameters
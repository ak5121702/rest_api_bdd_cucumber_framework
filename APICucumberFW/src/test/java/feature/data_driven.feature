Feature: Trigger post API on the basis of input data
@data_driven
Scenario Outline: Trigger the post API request with valid request parameters
                 Given "<Name>" and "<Job>" in request body
                 When send the request with payload to the endpoint
                 Then validate status code
                 And validate response body parameters
                 
Examples:
        
         |Name |Job |
         |akansha|Lead|
         |akku|QA|            
package StepDefinitions;

	import java.io.File;
	import java.io.IOException;

	import org.testng.Assert;

	import Common_Methods.API_Trigger;
	import Common_Methods.Utility;
	import Repository.RequestBody;
	import io.cucumber.java.en.Given;
	import io.cucumber.java.en.Then;
	import io.cucumber.java.en.When;
	import io.restassured.path.json.JsonPath;
	import io.restassured.response.Response;

public class Get_API_TestStep_Definition {
		Response response;
		int statuscode;
		int count;
		String requestBody;
		String Endpoint;
		String res_id;
		File dir_name;
		String res_body;
		int id[] = { 7, 8, 9, 10, 11, 12 };
		String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String firstname[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String lastname[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		
		
		@Given("configure endpoint for Get API")
		public void configure_endpoint() throws IOException {
			dir_name = Utility.CreateLogDirectory("Get_API_Logs");
			
			Endpoint = RequestBody.Hostname() + RequestBody.Resource_get ();
			response = API_Trigger.GET_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
					 Endpoint);
			Utility.evidenceGetFile(Utility.testLogName("Get_TC1"), dir_name, Endpoint,
					response.getHeader("Date"), response.getBody().asString());
			
		   
		}
		@When("send the request to the endpoint of Get API")
		public void send_the_request_to_the_endpoint() {
			res_body=response.getBody().asPrettyString();
			
			System.out.println(response.getBody().asPrettyString());
			statuscode = response.getStatusCode();
			System.out.println("statuscode is:" + " " + statuscode);

			JsonPath res_jsn = new JsonPath(res_body);
			count = res_jsn.getInt("data.size()");
			System.out.println("count of data array from response:" + " " + count);


			int id_Arr[] = new int[count];
			String email_Arr[] = new String[count];
			String firstname_Arr[] = new String[count];
			String lastname_Arr[] = new String[count];
			
			for (int i = 0; i < count; i++) {

				System.out.println("\n" + "-fetched array data from arrays of expected data-");

				
				
			int res_Id = res_jsn.getInt("data[" + i + "].id");
			System.out.println("\n" + res_Id);
			id_Arr[i] = res_Id;

			String res_Email = res_jsn.get("data[" + i + "].email");
			System.out.println(res_Email);
			email_Arr[i] = res_Email;

			String res_FirstName = res_jsn.getString("data[" + i + "].first_name");
			System.out.println(res_FirstName);
			firstname_Arr[i] = res_FirstName;

			String res_LastName = res_jsn.getString("data[" + i + "].last_name");
			System.out.println(res_LastName);
			lastname_Arr[i] = res_LastName;
		}


		   
		}
		@Then("validate status code for Get API")
		
		public void validate_status_code() {
			Assert.assertEquals(statuscode, 200);
		    
		}
		@Then("validate response body parameters for Get API")
		public void validate_response_body_parameters() {
			
			JsonPath res_jsn = new JsonPath(res_body);
			int id_Arr[] = new int[count];
			String email_Arr[] = new String[count];
			String firstname_Arr[] = new String[count];
			String lastname_Arr[] = new String[count];

			for (int i = 0; i < count; i++) {
				{
					System.out.println("\r\n" + "-fetched array data from arrays of expected data-");

					int res_Id = res_jsn.getInt("data[" + i + "].id");
					System.out.println("\n" + res_Id);
					id_Arr[i] = res_Id;

					String res_Email = res_jsn.get("data[" + i + "].email");
					System.out.println(res_Email);
					email_Arr[i] = res_Email;

					String res_FirstName = res_jsn.getString("data[" + i + "].first_name");
					System.out.println(res_FirstName);
					firstname_Arr[i] = res_FirstName;

					String res_LastName = res_jsn.getString("data[" + i + "].last_name");
					System.out.println(res_LastName);
					lastname_Arr[i] = res_LastName;
				}

			Assert.assertEquals(response.statusCode(), 200);

			Assert.assertEquals(id[i], id_Arr[i]);
			Assert.assertEquals(email[i], email_Arr[i]);
			Assert.assertEquals(firstname[i], firstname_Arr[i]);
			Assert.assertEquals(lastname[i], lastname_Arr[i]);
	    	Assert.assertEquals(statuscode, 200);
	  
		  
		}



		}}

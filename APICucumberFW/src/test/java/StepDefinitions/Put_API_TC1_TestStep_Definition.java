package StepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_API_TC1_TestStep_Definition {
	Response response;
	int statuscode;
	String requestBody;
	String Endpoint;
	String res_name;
	String res_job;
	String res_updatedAt;
	File dir_name;
	
	@Given("Name and Job in request body for Put_TC_1")
	public void name_and_job_in_request_body() throws IOException {
		dir_name = Utility.CreateLogDirectory("Put_API_Logs");
		requestBody = RequestBody.req_testcase2("Put_TC2");
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_put ();
		response = API_Trigger.PUT_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
				requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Put_TC2"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		
	   
	}
	@When("send the request with payload to the endpoint of Put_TC_1")
	public void send_the_request_with_payload_to_the_endpoint() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

	 
	}
	@Then("validate status code for Put_TC_1")
	
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 200);
	    
	}
	@Then("validate response body parameters for Put_TC_1")
	public void validate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

	
		Assert.assertEquals(response.statusCode(), 200);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt, expecteddate);

  
	  
	}



}
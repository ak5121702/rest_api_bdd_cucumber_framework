package StepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_API_TestStep_Definition {
	Response response;
	int statuscode;
	String requestBody;
	String Endpoint;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	File dir_name;
	
	@Given("{string} and {string} in request body")
	public void and_in_request_body(String req_name, String req_job) throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = "{\r\n" + "    \"name\": \""+req_name+"\",\r\n" + "    \"job\": \""+req_job+"\"\r\n" + "}";
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_post ();
		response = API_Trigger.POST_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
				requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Post_TC1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		
		
	}



	@Given("Name and Job in request body")
	public void name_and_job_in_request_body() throws IOException {
		dir_name = Utility.CreateLogDirectory("Post_API_Logs");
		requestBody = RequestBody.req_testcase1("Post_TC1");
		Endpoint = RequestBody.Hostname() + RequestBody.Resource_post ();
		response = API_Trigger.POST_trigger(RequestBody.Headername(), RequestBody.Headervalue(),
				requestBody, Endpoint);
		Utility.evidenceFileCreator(Utility.testLogName("Post_TC1"), dir_name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		
	  
	}
	@When("send the request with payload to the endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = res_body.jsonPath().getString("name");
		res_job = res_body.jsonPath().getString("job");
		res_id = res_body.jsonPath().getString("id");
		res_createdAt = res_body.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 11);
       System.out.println(res_createdAt);
	   
	}
	@Then("validate status code")
	
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
	    
	}
	@Then("validate response body parameters")
	public void validate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
        System.out.println(expecteddate);
	
		Assert.assertEquals(response.statusCode(), 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
    	Assert.assertEquals(res_createdAt, expecteddate);
  
	 
	}



}

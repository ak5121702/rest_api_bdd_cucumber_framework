package cucumber.Options;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/feature",glue= {"StepDefinitions"}, tags= "@data_driven or @Post_API_Testcases or @Put_API_Testcases or @Patch_API_Testcases or @Get_API_Testcases")
public class TestRunner {

}

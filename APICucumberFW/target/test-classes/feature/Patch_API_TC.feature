Feature: Trigger the Patch API and validate the response body and response parameters
@Patch_API_Testcases
Scenario: Trigger the Patch API request with valid string request body parameters
         Given Name and Job in request body for Patch API
         When send the request with payload to the endpoint of Patch API
         Then validate status code for Patch API
         And validate response body parameters for Patch API
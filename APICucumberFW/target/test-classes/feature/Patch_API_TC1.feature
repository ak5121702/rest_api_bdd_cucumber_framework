Feature: Trigger the Patch_TC_1 and validate the response body and response parameters
@Patch_API_Testcases
Scenario: Trigger the Patch_TC_1 API request with valid string request body parameters
         Given Name and Job in request body for Patch_TC_1
         When send the request with payload to the endpoint of Patch_TC_1
         Then validate status code for Patch_TC_1
         And validate response body parameters for Patch_TC_1
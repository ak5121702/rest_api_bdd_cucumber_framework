Feature: Trigger the Put API and validate the response body and response parameters
@Put_API_Testcases
Scenario: Trigger the Put API request with valid string request body parameters
         Given Name and Job in request body for Put API
         When send the request with payload to the endpoint of Put API
         Then validate status code for Put API
         And validate response body parameters for Put API
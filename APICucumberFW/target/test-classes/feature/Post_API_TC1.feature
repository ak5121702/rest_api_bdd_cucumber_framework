Feature: Trigger the Post_TC_1 API and validate the response body and response parameters
@Post_API_Testcases
Scenario: Trigger the Post_TC_1 API request with valid string request body parameters
         Given Name and Job in request body for Post_TC_1
         When send the request with payload to the endpoint of Post_TC_1
         Then validate status code for Post_TC_1
         And validate response body parameters for Post_TC_1
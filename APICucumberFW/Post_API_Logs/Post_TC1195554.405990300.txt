Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "akansha",
    "job": "Lead"
}

Response header date is : 
Sat, 16 Mar 2024 14:25:53 GMT

Response body is : 
{"name":"akansha","job":"Lead","id":"440","createdAt":"2024-03-16T14:25:53.727Z"}
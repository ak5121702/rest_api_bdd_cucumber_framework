Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "akku",
    "job": "QA"
}

Response header date is : 
Sat, 16 Mar 2024 14:21:06 GMT

Response body is : 
{"name":"akku","job":"QA","id":"799","createdAt":"2024-03-16T14:21:06.340Z"}
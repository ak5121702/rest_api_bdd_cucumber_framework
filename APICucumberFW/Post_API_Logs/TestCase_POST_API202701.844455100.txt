Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 14:57:02 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"289","createdAt":"2024-03-15T14:57:02.400Z"}
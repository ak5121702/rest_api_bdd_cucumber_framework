Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Fri, 15 Mar 2024 19:37:01 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"348","createdAt":"2024-03-15T19:37:01.294Z"}
Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sat, 16 Mar 2024 14:09:13 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"98","createdAt":"2024-03-16T14:09:13.349Z"}
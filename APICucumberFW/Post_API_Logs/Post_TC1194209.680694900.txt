Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "akansha",
    "job": "Lead"
}

Response header date is : 
Sat, 16 Mar 2024 14:12:09 GMT

Response body is : 
{"name":"akansha","job":"Lead","id":"949","createdAt":"2024-03-16T14:12:08.980Z"}
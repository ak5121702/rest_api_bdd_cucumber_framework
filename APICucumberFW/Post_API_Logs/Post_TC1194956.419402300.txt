Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "akansha",
    "job": "Lead"
}

Response header date is : 
Sat, 16 Mar 2024 14:19:55 GMT

Response body is : 
{"name":"akansha","job":"Lead","id":"651","createdAt":"2024-03-16T14:19:55.716Z"}
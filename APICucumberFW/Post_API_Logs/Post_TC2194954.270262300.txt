Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Akansha",
    "job": "QA"
}

Response header date is : 
Sat, 16 Mar 2024 14:19:53 GMT

Response body is : 
{"name":"Akansha","job":"QA","id":"883","createdAt":"2024-03-16T14:19:53.615Z"}
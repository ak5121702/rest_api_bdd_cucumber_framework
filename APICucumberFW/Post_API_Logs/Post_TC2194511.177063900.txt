Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Akansha",
    "job": "QA"
}

Response header date is : 
Sat, 16 Mar 2024 14:15:10 GMT

Response body is : 
{"name":"Akansha","job":"QA","id":"375","createdAt":"2024-03-16T14:15:10.539Z"}
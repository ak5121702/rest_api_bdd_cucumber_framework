Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "akansha",
    "job": "Lead"
}

Response header date is : 
Sat, 16 Mar 2024 14:09:16 GMT

Response body is : 
{"name":"akansha","job":"Lead","id":"255","createdAt":"2024-03-16T14:09:16.217Z"}
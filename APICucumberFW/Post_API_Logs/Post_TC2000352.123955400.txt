Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Akansha",
    "job": "QA"
}

Response header date is : 
Fri, 15 Mar 2024 18:33:52 GMT

Response body is : 
{"name":"Akansha","job":"QA","id":"551","createdAt":"2024-03-15T18:33:52.821Z"}
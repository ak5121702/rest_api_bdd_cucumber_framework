Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sat, 16 Mar 2024 14:25:50 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"622","createdAt":"2024-03-16T14:25:50.663Z"}
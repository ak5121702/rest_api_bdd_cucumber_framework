Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Akansha",
    "job": "QA"
}

Response header date is : 
Fri, 15 Mar 2024 18:42:05 GMT

Response body is : 
{"name":"Akansha","job":"QA","id":"718","createdAt":"2024-03-15T18:42:04.908Z"}
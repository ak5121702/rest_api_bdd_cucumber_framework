Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "akku",
    "job": "QA"
}

Response header date is : 
Sat, 16 Mar 2024 14:00:36 GMT

Response body is : 
{"name":"akku","job":"QA","id":"22","createdAt":"2024-03-16T14:00:36.455Z"}
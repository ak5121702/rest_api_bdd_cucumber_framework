Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sat, 16 Mar 2024 14:03:54 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"377","createdAt":"2024-03-16T14:03:54.534Z"}
Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "akku",
    "job": "QA"
}

Response header date is : 
Sat, 16 Mar 2024 14:19:56 GMT

Response body is : 
{"name":"akku","job":"QA","id":"735","createdAt":"2024-03-16T14:19:56.358Z"}
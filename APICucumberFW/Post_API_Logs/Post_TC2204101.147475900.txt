Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Akansha",
    "job": "QA"
}

Response header date is : 
Sat, 16 Mar 2024 15:11:00 GMT

Response body is : 
{"name":"Akansha","job":"QA","id":"235","createdAt":"2024-03-16T15:11:00.763Z"}
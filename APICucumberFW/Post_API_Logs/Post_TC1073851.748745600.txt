Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header date is : 
Sat, 16 Mar 2024 02:08:52 GMT

Response body is : 
{"name":"morpheus","job":"leader","id":"26","createdAt":"2024-03-16T02:08:52.098Z"}
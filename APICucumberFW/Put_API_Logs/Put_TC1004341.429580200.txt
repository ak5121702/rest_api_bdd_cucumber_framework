Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Raj",
    "job": "QA"
}

Response header date is : 
Fri, 15 Mar 2024 19:13:42 GMT

Response body is : 
{"name":"Raj","job":"QA","updatedAt":"2024-03-15T19:13:41.925Z"}
Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Jayesh",
    "job": "SrQA"
}

Response header date is : 
Fri, 15 Mar 2024 19:39:24 GMT

Response body is : 
{"name":"Jayesh","job":"SrQA","updatedAt":"2024-03-15T19:39:24.773Z"}
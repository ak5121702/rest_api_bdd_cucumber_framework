Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "Jayesh",
    "job": "SrQA"
}

Response header date is : 
Sat, 16 Mar 2024 14:15:12 GMT

Response body is : 
{"name":"Jayesh","job":"SrQA","updatedAt":"2024-03-16T14:15:12.098Z"}
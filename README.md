#Overview:
Cucumber framework is a popular open-source tool that facilitates the implementation of BDD practices. It allows teams to write executable specifications in Gherkin syntax and automate those specifications to verify the behavior of the software.

#Technologies Used:
[Rest Assured]: Core API testing library.
[Java]: Programming language for building the project.
[Maven]: Dependency management and build tool.

#Instructions:
Creating a project using Java RestAssured  Framework.
Steps for testing:-
Create a Java project
To configure all the Rest APIs:
-Execute it
-Extract response (Utility used - RestAssured )
-Parse the response (Utility used - JsonPath )
-Validate the response (Used Cucumber Library)
-Read data from excel file (Used ApachePOI)

#Steps:
1. Write Feature Files: Define test scenarios in feature files using Gherkin syntax. Feature files describe the behavior of the API endpoints from an end-user perspective.

2. Implement Step Definitions: Write step definitions in Java to map the steps described in the feature files to executable code. In these step definitions, you'll use REST Assured to send HTTP requests, validate responses, and perform assertions.

3. Execute Tests with Cucumber: Use Cucumber to execute the tests defined in the feature files. Cucumber will run the scenarios and invoke the corresponding step definitions to perform the actual testing.


